# Changelog

## v0.2.1, 12-08-2020
- Fixed ordering on plot_function().

## v0.2.0, 09-08-2020
- Added the ability to run multiple times with the same execution name, when running this way the mean time will be plotted along with the standard deviation;
- Added log-log scale to functions plot.

## v0.1.1, 08-22-2020
- Removed numpy dependency;
- Some organization on the code.

## v0.1.0, 08-22-2020
- Initial release;
- Branched profiler from [COMORBUSS](https://gitlab.com/ggoedert/comorbuss).