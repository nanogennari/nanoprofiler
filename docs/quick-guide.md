# Quick guide

## Instalation

nanoProfiler can be installed with pip:

    pip install nanoprofiler

or:

    python -m pip install nanoprofiler

### Manual instalation

Clone the repository

    git clone https://gitlab.com/nanogennari/nanoprofiler.git

And run setup script

    cd nanoprofiler
    python setup.py install

!!! tip
    To install nanoprofiler via `setup.py` you will need `setuptools` installed in your machine, on most distributions this can be achieved by installing the `python-setuptools` package.

## Usage

nanoProfiler was primarily designed evaluate the internal variations of execution times in codes with variable complexity. But it can also be used with fixed complexity code.

### Activating and deactivating the profiler

The profiler can be activated with [`Profiler.start()`](../reference/#nanoprofiler.profiler.Profiler.start) and deactivated with [`Profiler.stop()`](../reference/#nanoprofiler.profiler.Profiler.stop). User can also instance a cProfile object with [`Profiler.set_cprofiler()`](../reference/#nanoprofiler.profiler.Profiler.set_cprofiler) for multiple activations of the profiler in a single execution, use cProfile methods `enable()` and `disable()` and at the end of the execution call [`Profiler.stop()`](../reference/#nanoprofiler.profiler.Profiler.stop) to process and store results.

!!! Important
    Everything executed between a [`Profiler.start()`](../reference/#nanoprofiler.profiler.Profiler.start) or [`Profiler.set_cprofiler()`](../reference/#nanoprofiler.profiler.Profiler.set_cprofiler), and [`Profiler.stop()`](../reference/#nanoprofiler.profiler.Profiler.stop) is considered a single execution, [`Profiler.start()`](../reference/#nanoprofiler.profiler.Profiler.start) and [`Profiler.set_cprofiler()`](../reference/#nanoprofiler.profiler.Profiler.set_cprofiler) can accept a `name` keyword parameter to identify the execution, if the same name is used multiple times the plots will contain the mean of the multiple executions.

### Ploting results

!!! tldr
    To see results from individual executions use [`Profiler.plot_top_time()`](../reference/#nanoprofiler.profiler.Profiler.plot_top_time), and to see variations between multiple executions use [`Profiler.plot_function()`](../reference/#nanoprofiler.profiler.Profiler.plot_function).

Results can be ploted with [`Profiler.plot_top_time()`](../reference/#nanoprofiler.profiler.Profiler.plot_top_time) that will plot a bar graph with the top functions for each execution, or with [`Profiler.plot_function()`](../reference/#nanoprofiler.profiler.Profiler.plot_function) to plot difference in execution time between multiple executions for selected functions.


### Storing and loading results

Results can be stored in .csv files with [`Profiler.save_results()`](../reference/#nanoprofiler.profiler.Profiler.save_results) where one .csv file will be created for each execution, and loaded with [`Profiler.load_results()`](../reference/#nanoprofiler.profiler.Profiler.load_results)

### Usage example

    from nanoprofiler import Profiler

    pr = Profiler()

    pr.start(name="exec1")
    your_code()
    pr.stop()

    pr.start(name="exec2")
    your_code_again()
    pr.stop()

    pr.plot_top_time(time="cumtime")
    pr.plot_function(time="tottime")
    pr.save_data("folder/to/save/results", "prefix_for_files")